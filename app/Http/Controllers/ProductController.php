<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function store (Request $request){
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'name'=>['required'],
            'image'=>['required'],
            'description'=>['required'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $product = Product::create([
            'name' => $request->name,
            'image' => $request->image,
            'description' => $request->description,
        ]);

        return response(['message'=> 'Product created Succcessfully',
                        'product'=>$product
                    ], 200);
    }

    public function update (Request $request){

        $product = Product::find($request->id);

        if(!$product){
            return response(['message'=> 'Product does not exist'
                    ], 401);
        }

        $product->update($request->all());

        return response(['message'=> 'Product updated Succcessfully'
                    ], 200);
    }

    public function destroy (Request $request){

        $product = Product::find($request->id);

        if(!$product){
            return response(['message'=> 'Product does not exist'
                    ], 401);
        }

        Product::destroy($request->id);

        return response()->json(['message'=> 'Product deleted Succcessfully'
                    ], 200);
    }

    public function index(){
        $products = Product::all();

        if(!$products){
            return response()->json(['message'=> 'There are no products'
                    ], 200);
        }

        return response()->json(['products'=> Product::paginate(4)
                    ], 200);
    }

    public function find (Request $request){

        $product = Product::find($request->id);

        if(!$product){
            return response(['message'=> 'Product does not exist'
                    ], 401);
        }

        return response()->json(['product'=> $product
                    ], 200);
    }
}
