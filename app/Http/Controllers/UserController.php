<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Models\User;
use Nette\Utils\Random;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function storeByPhone(Request $request)
    {
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required'],
            'phone' => ['required','string', 'unique:users']
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user = User::forceCreate([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'email_verified_at'=>Carbon::now()
        ]);

        return response(['message'=> 'User created Succcessfully',
                        'token'=>$user->createToken("Digital")->plainTextToken
                    ], 200);
    }

    public function storeByEmail(Request $request)
    {
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $code = Random::generate(4, '0-9');

        $user = User::forceCreate([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'email_code'=>$code
        ]);

        //send the email containing a code
        /*

        */

        return response(['message'=> 'User created Succcessfully but needs an activation'
                        ], 200);
    }

    public function activateUser(Request $request){

        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'email' => ['required', 'string', 'email', 'max:255'],
            'email_code' => ['required', 'max:4', 'min:4'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user = User::where('email', '=',$request->email)->first();

        if($user->email_code==$request->email_code){
            $user->email_verified_at = Carbon::now();
            $user->email_code=null;
            $user->save();
            return response(['message'=>'user activated successfully'], 200);
        }

        return response(['message'=>'user is not activated. please check the code'], 200);
    }

    public function loginByEmail(Request $request){
        $validateUser = Validator::make($request->all(), 
            [
                'email' => 'required|string|email|max:255',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if(!Auth::attempt($request->only(['email', 'password']))){
                return response()->json([
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("Digital")->plainTextToken
            ], 200);
    }

    public function loginByPhone(Request $request){
        $validateUser = Validator::make($request->all(), 
            [
                'phone' => 'required',
                'password' => 'required'
            ]);

            if($validateUser->fails()){
                return response()->json([
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if(!Auth::attempt($request->only(['phone', 'password']))){
                return response()->json([
                    'message' => 'phone & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('phone', $request->phone)->first();

            return response()->json([
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("Digital")->plainTextToken
            ], 200);
    }

    public function find(Request $request){
        $user = User::find($request->id);
        if($user){
            return response()->json([
                'user' => $user,
            ], 200);
        } else {
            return response()->json([
                'message' => 'There\'s no such user' ,
            ], 200);
        }
        
    }

    public function update(Request $request){

        $user=User::find($request->id);
        //user is not found
        if(!$user){
            return response()->json([
                'message' => 'There\'s no such user' ,
            ], 200);
        }

        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'first_name' => ['string', 'max:255'],
            'last_name' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255', 'unique:users'],
            'phone' => ['string', 'unique:users']
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user->update($request->all());

        return response(['message'=> 'User Updated Succcessfully',
                    ], 200);
    }

    public function destroy(Request $request){
        $user = User::find($request->id);
        if($user){
            User::destroy($request->id);
            return response()->json([
                'message' => 'user deleted successfully',
            ], 200);
        } else {
            return response()->json([
                'message' => 'There\'s no such user' ,
            ], 200);
        }
        
    }

    public function index(){
        return response()->json(['users'=>User::all()],200);
    }

    public function changePassword(Request $request){
        $user=Auth::user();

        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'current_password'=>['required'],
            'new_password' => ['required', 'different:current_password'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        if(!Hash::check($request->current_password, $user->password)){
            return response([
                'message' => 'current password not correct'
            ], 401);
        }

        $user->password=Hash::make($request->new_password);
        $user->save();

        return response([
            'message' => 'password changed successfully'
        ], 401);
    }

    public function getUserProducts(){
        return response([
            'products' => Auth::user()->products()->paginate(4)
        ], 401);
    }

    public function assignProducts(Request $request){
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'user_id'=>['required'],
            'products' => ['required'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user=User::find($request->user_id);
        if(!$user){
            return response([
                'message' => 'user does not exist',
            ], 401);
        }

        foreach ($request->products as $product){
            if(!Product::find($product)){
                return response([
                    'message' => 'product with id '.$product.' does not exists',
                ], 401);
            }
        }

        foreach ($request->products as $product){
            $user->products()->attach($product);
        }

        return response([
            'message' => 'all products have been assigned',
        ], 200);
    }

    public function forgotPassword(Request $request){
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'email'=>['required', 'string', 'email', 'max:255'],
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user=User::where('email', '=', $request->email)->first();
        if($user){
            $code=Random::generate(4);
            $user->email_code=$code;
            $user->save();

            //send the code via email
        }

        return response([
            'message' => 'we have sent you an email if such account exists',
        ], 200);
    }

    public function resetPassword(Request $request){
        //Validated
        $validateUser = Validator::make($request->all(), 
        [
            'email'=>['required', 'string', 'email', 'max:255'],
            'code'=>['required'],
            'password'=>['required']
        ]);

        if($validateUser->fails()){
            return response([
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        $user=User::where('email', '=', $request->email)->first();
        if($user){
            if($user->email_code==$request->code){
                $user->password=Hash::make($request->password);
                $user->email_code=null;
                $user->save();
                return response([
                    'message' => 'password has been resetted successfully',
                ], 200);
            }
        }

        return response([
            'message' => 'something went wrong, please check email & code again',
        ], 401);
    }
}
