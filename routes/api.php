<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\AdminMiddleware;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function(){
    Route::put('/changePassword', [UserController::class, 'changePassword']);
    Route::get('/user/products', [UserController::class, 'getUserProducts']);
});

Route::middleware([AdminMiddleware::class, 'auth:sanctum'])->group(function (){
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/users/{id}', [UserController::class, 'find']);
    Route::put('/users/{id}', [UserController::class, 'update']);
    Route::delete('/users/{id}', [UserController::class, 'destroy']);
    Route::post('/products', [ProductController::class, 'store']);
    Route::put('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
    Route::post('/assignProducts', [UserController::class, 'assignProducts']);
});


Route::get('/products/{id}', [ProductController::class, 'find']);
Route::get('/products', [ProductController::class, 'index']);

Route::post('/registerByPhone', [UserController::class, 'storeByPhone']);
Route::post('/registerByEmail', [UserController::class, 'storeByEmail']);
Route::post('/activateUser', [UserController::class, 'activateUser']);
Route::post('/loginByPhone', [UserController::class, 'loginByPhone']);
Route::post('/loginByEmail', [UserController::class, 'loginByEmail']);
Route::post('/forgotPassword', [UserController::class, 'forgotPassword']);
Route::post('/resetPassword', [UserController::class, 'resetPassword']);

